import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('TkAgg')
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.animation as animation

NROWS = 18
SHEET_TITLE = 'Instat1'
SKIP_ROWS = 3
YEARS = 21

population = pd.read_excel('data.xlsx', sheet_name=SHEET_TITLE, skiprows=SKIP_ROWS, nrows=NROWS)

Writer = animation.writers['imagemagick']
writer = Writer(fps=10, bitrate=2400)

sns.set(rc={'axes.facecolor':'lightgrey', 'figure.facecolor':'lightgrey','figure.edgecolor':'black','axes.grid':False})


def animate(i):
    data = frame.iloc[:int(i+1)] #select data range
    p = sns.lineplot(x=data.index, y=data[title], data=data, color="r")
    p.tick_params(labelsize=15)
    plt.setp(p.lines, linewidth=5)


def get_data(table, rownum, colnum, title):
    data = pd.DataFrame(table.loc[rownum][colnum:]).astype(float)
    data.columns = {title}
    return data


for row in range(NROWS):
    title = 'Grupmosha ' + population.loc[row][1] + ' vjec'
    d = get_data(population, row, 2, title)
    x = np.array(d.index).astype(int)
    y = np.array(d[title])
    frame = pd.DataFrame(y, x)
    frame.columns = {title}

    fig = plt.figure(figsize=(10,6))
    plt.xlim(np.min(x), np.max(x)+1)
    plt.ylim(0, np.max(y))
    plt.xlabel('Viti', fontsize=20)
    plt.xticks(np.arange(min(x), max(x)+1, 2.0))
    plt.yticks(np.arange(0, max(y)+25000, 25000))
    plt.title(title, fontsize=20)

    ani = matplotlib.animation.FuncAnimation(fig, animate, frames=YEARS)
    ani.save(title+'.gif', writer=writer)